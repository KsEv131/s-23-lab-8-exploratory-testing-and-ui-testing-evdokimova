# Testcases

## Probable testcases (Future Testing Opportunities)

- A customer browse flight ticket by parameters
- A customer browse flight places
- A customer select the flight
- A customer paying with a personal credit card
- A customer paying with UpCloud loyalty miles
- A customer browse hotels
- etc.  

## Use Case

### A customer browse flight ticket by parameters (flight dates: start, end, # of passengers, departure and destination cities)

--------------------------
| | Use Case 1 | | |
|-------------|------------------------------------------------------------------|--------|------------------------------------------------------------------------------------------------------------------------------|
| Test Tour |  | 
| Object to be tested/Rough Guidance | Browse a suitable flight by parameteres |
| Test Duration (Execution) | 117.592s *(I added Explicit waits & time.sleep() to avoid problems with not found elements)* |
| Tester | Kseniya Evdokimova |
| Further Testing Opportunities | (illustrated above). Also, in this testcase, we can test providing unsuitable values into input field or change or exceed tha max number of passengers, etc. |
-----------------------------  

## Protocol

---------------------
| Step Number | What Done                                                        | Status | Comment                                                                                                                      |
|-------------|------------------------------------------------------------------|--------|------------------------------------------------------------------------------------------------------------------------------|
| 1 | Navigate to https://www.aviasales.ru/ | Pass   | The page loaded successfully. |
| 2 | Verify the presence of the word "Авиасейлс" in the title    | Pass   | It is present on the page.  |
| 3 | Verify the presence of the header form   | Pass   | Search bar is present on the page.  |
| 4 | Verify the presense of each input-field: origin place, destination place, time borders of the flight date (to/from), additional info field (regarding the # of passengers+price case), button | Pass | Each of 6 elements is present on the page |
| 4 | Try hitting the submit button without providing any values for the inputs | Pass | Hints to provide values are present on the page |
| 5 | Provide (choose) value for the origine place = "Казань" | Pass | "Казань" is typed into the input-field successfully |
| 6 | Provide (choose) value for the destination place = "Москва" | Pass | "Москва" is typed into the input-field successfully |
| 7 | Provide value for the start date = "07.05.2023" | Pass | We execute click event o the input field and then of the future date in the calendar. Success |
| 8 | Try hitting the submit button after providing required values for the inputs | Pass | Success. The next page with suitable flights starts loading |
| 9 | Check that the next page with proposed flights is loaded. Check it by searching "Следить за ценой" text | Pass | It's found |
------------------------