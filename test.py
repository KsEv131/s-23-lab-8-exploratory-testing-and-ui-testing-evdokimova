import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from time import sleep


class LandmarkTour(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_title(self):
        driver = self.driver
        driver.implicitly_wait(10) # seconds
        # Step 1: Navigate to aviasales
        driver.get("https://www.aviasales.ru/")
        # Step 1. Assert that the name of the company is in the title
        self.assertIn("Авиасейлс", driver.title)

    def test_header_form(self):
        driver = self.driver
        driver.implicitly_wait(15) # seconds
        driver.get("https://www.aviasales.ru/")
        # Step 2. Verify the presence of the header form
        ticket_form = driver.find_elements(By.TAG_NAME, "form")
        assert len(ticket_form)>0
    
    def test_input_fields(self):
        driver = self.driver
        driver.implicitly_wait(10) # seconds
        driver.get("https://www.aviasales.ru/")
        sleep(5)
        # Step 3.1. Verify the presense of origin place input-field
        xpath_origin = "//input[contains(@data-test-id, 'origin-autocomplete-field')]"
        origin_place = driver.find_elements(By.XPATH, xpath_origin)
        assert len(origin_place)>0
        # Step 3.2. Verify the presense of destination place input-field
        xpath_dest = "//input[contains(@data-test-id, 'destination-autocomplete-field')]"
        destination_place = driver.find_elements(By.XPATH, xpath_dest)
        assert len(destination_place)>0
        sleep(5)
        # Step 3.3. Verify the presense of start date field
        xpath1 ="//button[@data-test-id='start-date-field']"
        start_date = driver.find_element(By.XPATH, xpath1)
        assert start_date.is_displayed()
        # Step 3.4. Verify the presense of end date field
        xpath2 ="//button[contains(@data-test-id, 'end-date-field')]"
        end_date = driver.find_element(By.XPATH, xpath2)
        assert end_date.is_displayed()
        # Step 3.5. Verify the presense of passengers&class field
        xpath3 ="//button[@data-test-id='form-dropdown-field']"
        add_info = driver.find_element(By.XPATH, xpath3)
        assert add_info.is_displayed()
        # Step 3.6. Verify the presense of submit button
        xpath4 ="//button[contains(@data-test-id, 'form-submit')]"
        submit_field = driver.find_element(By.XPATH, xpath4)
        assert submit_field.is_displayed()

    def test_find_ticket_empty_params(self):
        driver = self.driver
        driver.implicitly_wait(15) # seconds
        driver.get("https://www.aviasales.ru/")
        sleep(5)
        # Step 4. Empty input fields -> Hints to provide them
        wait = WebDriverWait(driver, 10)
        xpath ="//button[contains(@data-test-id, 'form-submit')]"
        submit_button = wait.until(EC.element_to_be_clickable((By.XPATH, xpath)))
        driver.execute_script("arguments[0].click();", submit_button)
        assert "Укажите" in driver.page_source

    def test_find_ticket_provided_params(self):
        driver = self.driver
        driver.implicitly_wait(10) # seconds
        driver.get("https://www.aviasales.ru/")
        sleep(10)
        # Step 5. Provide info for required input-fields
        # where from
        origin = driver.find_element(By.ID, "avia_form_origin-input")
        origin.clear()
        origin.send_keys("Казань")
        origin.send_keys(Keys.RETURN)
        # to where
        destination = driver.find_element(By.ID, "avia_form_destination-input")
        destination.send_keys("Москва")
        destination.send_keys(Keys.RETURN)
        # when
        wait = WebDriverWait(driver, 10)
        xpath_start_field ="//button[contains(@data-test-id, 'start-date-field')]"
        start_date_field = wait.until(EC.element_to_be_clickable((By.XPATH, xpath_start_field)))
        driver.execute_script("arguments[0].click();", start_date_field)
        xpath_start_date ="//div[contains(@data-test-id, 'date-07.05.2023')]"
        start_date = wait.until(EC.element_to_be_clickable((By.XPATH, xpath_start_date)))
        driver.execute_script("arguments[0].click();", start_date)
        # submit
        xpath_submit ="//button[contains(@data-test-id, 'form-submit')]"
        submit_button = wait.until(EC.element_to_be_clickable((By.XPATH, xpath_submit)))
        driver.execute_script("arguments[0].click();", submit_button)
        sleep(5)
        assert "Следить за ценой" in driver.page_source

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
    
